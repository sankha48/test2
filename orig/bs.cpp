#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "conio.h"
using namespace std;

#define MAX_TITLE 48
#define MAX_AUTHOR_NAME 32

char ALL_BOOKS_FILE_NAME[64] = "allbks.dat";

class Book {
  // Book data members
  char category[8];
  char id[16];
  char title[MAX_TITLE];
  char author[MAX_AUTHOR_NAME];
  double price;

  public:
    // Constructor for Book class
    Book() {}

  void getData() {
    do {
      cout << "Enter category: M(Maths), P(Physics), C(Chemistry):" << endl;
      cin.getline(category, 8);
      if (strcmp(category, "M") != 0 &&
        strcmp(category, "P") != 0 &&
        strcmp(category, "C") != 0) {
        cout << "Invalid data" << endl;
      } else break;
    }
    while (1);

    do {
      cout << "Enter title:" << endl;
      cin.getline(title, MAX_TITLE);
      if (strlen(title) == 0) {
        cout << "Invalid Author Name" << endl;
      } else break;
    } while (1);

    do {
      cout << "Enter author:" << endl;
      cin.getline(author, MAX_AUTHOR_NAME);
      if (strlen(author) == 0) {
        cout << "Invalid Author Name" << endl;
      } else break;
    }
    while (1);

    cout << "Enter price:" << endl;
    cin >> price;
    cin.clear();
    cin.ignore();
    time_t t = time(0);
    char dt[32];
    strftime(dt, 20, "%d%m%y%H%M%S", localtime( & t));
    sprintf(id, "%s%s", category, dt);
  }

  void showData() {
    cout << category;
    cout << title;
    cout << author;
    cout << id;
  }
  char * getCategory() {
    return category;
  }
  char * getTitle() {
    return title;
  }
  char * getId() {
    return id;
  }
  char * getAuthor() {
    return author;
  }
  double getPrice() {
    return price;
  }
};

class Item {
  public:
    Item * next;
  Book data;
  int numCopies;
  Item() {}
};

// This class maintains a list of books and provides the necessary
// methods for
class ItemList {
  public:
    Item * head;

  //constructor
  ItemList() {
    head = NULL;
  };

  //returns 1 / 0 if list has items or not
  int hasItems() {
    return (head != NULL) ? 1 : 0;
  }

  // lists all items in the list
  void list() {
    Item * head = this->head;
    int num = 0;
    while (head) {
      num++;
      Book* b = &head->data;
      cout << num << ". " << b->getTitle() << ", " << b->getAuthor() << ", " << b->getPrice() << endl;
      head = head->next;
    }
  }
  // adds a book item to the list
  void add(Book& data, int numCopies) {
    Item* node = new Item();
    node->data = data;
    node-> numCopies = numCopies;
    node-> next = this-> head;
    this-> head = node;
  }

  // updates a book item if found, else adds a new entry
  void update(Book data, int numCopies) {
    int found = 0;
    Item* cur = this->head;
    while (cur) {
      if (strcmp(data.getId(), cur->data.getId()) == 0) {
	cur->numCopies = numCopies;
	found = 1;
	break;
      }
      cur = cur->next;
    }
    if (found == 0) {
      add(data, numCopies);
    }
  }
  // clears all the items in the list
  void clearItems() {
    Item* current = this->head;
    Item* next;

    while (current != NULL) {
      next = current->next;
      delete current;
      current = next;
    }
    head = NULL;
  }

  // searches a book item from the list by the serial number
  Book* getItem(int i) {
    Item* head = this->head;
    int num = 0;
    while (head) {
      num++;
      if (i == num) {
	return &head->data;
      }
      head = head->next;
    }
    return NULL;
  }

  // calculates and prints the bill from the list of books
  void printBill() {
    int num = 0, sum = 0;
    char dt[32];

    Item* head = this->head;

    if (this-> head == NULL) {
      cout << "No purchases yet" << endl;
      return;
    }

    time_t t = time(0);

    strftime(dt, 20, "%d.%m.%y %H:%M:%S", localtime( &t));
    cout << "=====================================================================" << endl;
    cout << "BILL: " << dt << endl;
    cout << "=====================================================================" << endl;
    while (head) {
      num++;
      int n = head->numCopies;
      int amt = head->data.getPrice() * n;
      cout << num << ". " << setw(40) << head->data.getTitle() << " (" << setw(2) << n << " ) ....    Rs " << setw(5) << amt << endl;
      sum += amt;
      head = head->next;
    }
    cout << "=====================================================================" << endl;
    cout << "TOTAL: Rs " << sum << endl;
    cout << "=====================================================================" << endl;
  }
};

// This class is used for add/update/removal from book database
class BkMgr {
  char fname[64];
  public:
    //constructor
    BkMgr(char * fname) {
      strcpy(this->fname, fname);
    }
  void addBook() {
    fstream ifs;
    ifs.open(fname, ios::binary | ios::app);
    Book b;
    char ch;

    while (1) {
      b.getData();
      cout << "Add Book(Y)/Reject(N)" << endl;
      cin >> ch;
      if (ch == 'Y' || ch == 'y')
	    ifs.write((char * ) &b, sizeof(b));

      cout << "More?" << endl;
      cin >> ch;
      if (ch == 'N' || ch == 'n') break;
    }

    ifs.close();
  }

  void list() {
    ifstream ifs;
    ifs.open(fname, ios::binary);
    Book b;
    while (ifs.read((char * ) &b, sizeof(b))) {
      cout << b.getTitle() << ", author=" << b.getAuthor() << ", Rs  " << b.getPrice() << ", id=" << b.getId() << endl;
    }

    ifs.close();
  }

  void removeBook() {
    char itemId[32];
    cout << "Enter item id to remove" << endl;
    cin >> itemId;

    ifstream ifs;
    ofstream ifs2;
    ifs.open(fname, ios::binary);
    ifs2.open("tmpbks.dat", ios::out | ios::binary);
    Book b;
    while (ifs.read((char * ) & b, sizeof(b))) {
      if (strcmp(b.getId(), itemId) != 0)
	ifs2.write((char * ) & b, sizeof(b));
    }

    ifs.close();
    ifs2.close();
    remove(fname);
    rename("tmpbks.dat", fname);
  }

  static ItemList * searchByTitle(char * fname, char * category, char * title) {
    int ret = 0;
    ifstream ifs;
    ifs.open(fname, ios::binary);
    Book b;
    ItemList * result = new ItemList();
    while (ifs.read((char * ) &b, sizeof(b))) {
      if (strcmp(category, b.getCategory()) != 0) continue;
      if (keywordSearch(b.getTitle(), title) == 0) {
	result->add(b, 0);
      }
    }

    ifs.close();
    return result;
  }

  static int keywordSearch(char * bookTitle, char * q) {

    char k[64] = { '\0' };
    int j = 0;
    // test if a single keyword in the searchStr is present in bookTitle
    for (int i = 0; i < strlen(bookTitle); i++) {
      char x = bookTitle[i];
      if (x != ' ')
	k[j++] = x;
      else {
	k[j] = '\0';
	if (strcmpi(q, k) == 0) return 0;

	j = 0;
      }
    }
    if (j > 0) {
      k[j] = '\0';
      if (strcmpi(q, k) == 0) return 0;
    }
    return -1;
  }

};

// This function gets invoked when user chooses the Admin option
void adminMenu() {
  BkMgr ob(ALL_BOOKS_FILE_NAME);
  do {
    cout << endl;
    cout << "    ====ADMIN MENU====" << endl;
    cout << "    LIST BOOKS(L)" << endl;
    cout << "    ADD BOOK(A)" << endl;
    cout << "    REMOVE BOOK(R)" << endl;
    cout << "    BACK(B)" << endl;
    cout << "    ==================" << endl;
    char c;
    cin >> c;

    cin.clear();
    cin.ignore();

    if (c == 'l' || c == 'L') {
      ob.list();
    } else if (c == 'a' || c == 'A') {
      ob.addBook();
    } else if (c == 'r' || c == 'R') {
      ob.removeBook();
    } else if (c == 'b' || c == 'B') {
      break;
    }
  } while (1);
}

// This function gets invoked when user chooses the Choose Books option
void chooseBooks(ItemList * cart) {
  char category[64];
  char stype[64];
  do {
    cout << "Enter category M(Maths), P(Physics), C(Chemistry):" << endl;
    gets(category);
    if (strcmp(category, "M") != 0 &&
      strcmp(category, "P") != 0 &&
      strcmp(category, "C") != 0) {
      cout << "Invalid data" << endl;
    } else break;
  } while (1);

  int ret = -1;
  char author[64];
  int num = 0, numCopies = 0;
  ItemList * searchResult = NULL;
  char title[64];
  cout << "ENTER KEYWORD IN BOOK TITLE" << endl;
  cin >> title;
  searchResult = BkMgr::searchByTitle(ALL_BOOKS_FILE_NAME, category, title);

  if (searchResult->hasItems() == 1) {
    searchResult->list();
    Book * b = NULL;
    do {
      cout << "ENTER THE SERIAL NUMBER OF THE BOOK YOU WANT TO BUY" << endl;
      cin >> num;
      if (num <= 0) {
	cout << "INVALID SERIAL NUMBER" << endl;
	continue;
      }
      b = searchResult->getItem(num);
      if (b == NULL) {
	cout << "BOOK NOT FOUND" << endl;
      } else {
	break;
      }
    } while (1);

    do {
      cout << "ENTER THE NUMBER OF COPIES" << endl;
      cin >> numCopies;
      if (numCopies < 0 || numCopies > 10) {
	cout << "INVALID NUMBER OF COPIES" << endl;
	continue;
      } else break;
    } while (1);
    cart-> update(*b, numCopies);
    searchResult->clearItems();
    delete searchResult;
  }

}

// This function gets invoked when user chooses to generate bill option
void generateBill(ItemList * cart) {
  char ch[128];
  cart->printBill();
  cout << "Press any key" << endl;
  getchar();
}

// This function gets invoked when user chooses the Fresh Buy option
void freshBuyMenu() {
  ItemList cart;

  do {
    cout << "    ===FRESH BUY===" << endl;
    cout << "    CHOOSE BOOKS(C)" << endl;
    cout << "    GENERATE BILL(G)" << endl;
    cout << "    BACK(B)" << endl;
    cout << "    ===============" << endl;
    char c;
    cin >> c;
    cin.ignore();
    if (c == 'c' || c == 'C') {
      chooseBooks( & cart);
    } else if (c == 'g' || c == 'G') {
      generateBill( & cart);
    } else if (c == 'b' || c == 'B') {
      break;
    }
  } while (1);

}

// This function guarantees that the only admin
// can add/remove books using password validation. 
int matchPasswd() {
  const char * ADMIN_PASSWD = "abcd1234";
  char pass[32], ch;
  int i = 0;
  cout << "Enter password: ";
  ch = getch();
  while (ch != 13) { //character 13 is enter
    pass[i++] = ch;
    cout << '*';
    ch = getch();
  }
  pass[i] = '\0';
  return strcmp(pass, ADMIN_PASSWD);
}

// Main function
int main() {
  do {
    cout << "        ====MAIN MENU====" << endl;
    cout << "        ADMIN(A)" << endl;
    cout << "        FRESH BUY(F)" << endl;
    cout << "        EXIT(E)" << endl;
    char c;
    cin >> c;
    if (c == 'e' || c == 'E') {
      break;
    } else if (c == 'a' || c == 'A') {
      if (matchPasswd() == 0)
        adminMenu();
      else
        cout << "Incorrect Password..." << endl;
    } else if (c == 'f' || c == 'F') {
      freshBuyMenu();
    }
  } while (1);
  return 0;
}

